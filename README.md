
# Web PT Test

## Running the Application
This is a standard spring boot application, it can be ran from the command line with the command:

`./gradlew bootRun`

## Available Calls

#### Request

`GET /find/{amount}`

##### Variables

| Property          | Description                                                       | Required |
|-------------------|-------------------------------------------------------------------| :-------:|
| amount            | The amount of fields we will attempt to retrieve from the server  | Yes      |


#### Response
The response will contain an aggregate of all the payloads we requested

 ```
[
    {
        "damage": 18,
        "method": "Tesla Cannon",
        "platform": "pc",
        "region": "us",
        "source_character": "Winston",
        "source_player_id": "Jimmy#222",
        "target_character": "Zenyatta",
        "target_player_id": "FalconPunch#133"
    },
    {
        "damage": 61,
        "method": "Orb of Discord",
        "platform": "pc",
        "region": "us",
        "source_character": "Zenyatta",
        "source_player_id": "FalconPunch#133",
        "target_character": "Winston",
        "target_player_id": "Jimmy#222"
    }
 ```

#### Request

`GET /aggregate/character/{amount}`

##### Variables

| Property          | Description                                                       | Required |
|-------------------|-------------------------------------------------------------------| :-------:|
| amount            | The amount of fields we will attempt to retrieve from the server  | Yes      |


#### Response
The response will contain each source character that is found with a summation of their damage.

 ```
[
    {
        "damage": 66,
        "sourceCharacter": "Mei"
    },
    {
        "damage": 19,
        "sourceCharacter": "Winston"
    },
    {
        "damage": 64,
        "sourceCharacter": "Zenyatta"
    }
]
 ```
 
 #### Request
 
 `GET /find/methods/{amount}`
 
 ##### Variables
 
 | Property          | Description                                                       | Required |
 |-------------------|-------------------------------------------------------------------| :-------:|
 | amount            | The amount of fields we will attempt to retrieve from the server  | Yes      |
 
 
 #### Response
 The response will find all the methods used and the amount of times that method is used from highest to lowest.
 
  ```
 [
    {
        "amount": 10,
        "method": "Icicle"
    },
    {
        "amount": 9,
        "method": "Cryo-Freeze"
    },
    {
        "amount": 8,
        "method": "Primal Rage"
    },
    {
        "amount": 8,
        "method": "Orb of Discord"
    }
 ]
  ```