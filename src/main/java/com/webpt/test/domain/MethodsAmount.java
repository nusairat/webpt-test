package com.webpt.test.domain;

public class MethodsAmount {
    public String method;
    public long amount;

    public MethodsAmount(String method, long amount) {
        this.method = method;
        this.amount = amount;
    }

    public long getAmount() {
        return amount;
    }
}
