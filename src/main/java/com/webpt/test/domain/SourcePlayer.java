package com.webpt.test.domain;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public class SourcePlayer {
    public String sourceCharacter;
    public int damage = 0;

    public SourcePlayer(String sourceChar, int dam) {
        sourceCharacter = sourceChar;
        damage = dam;
    }

    public String getSourceCharacter() {
        return sourceCharacter;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
