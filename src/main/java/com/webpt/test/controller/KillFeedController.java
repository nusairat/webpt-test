package com.webpt.test.controller;

import com.webpt.test.domain.MethodsAmount;
import com.webpt.test.domain.SourcePlayer;
import com.webpt.test.model.Payload;
import com.webpt.test.service.RetrieveRecordsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.Future;

@RestController
public class KillFeedController {

    Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired
    RetrieveRecordsService service;

    @GetMapping("/find/{amount}")
    public Future<List<Payload>> aggregatePayloads(@PathVariable("amount")Integer amount) {
        LOG.info("Find the aggregates for {}", amount);
        return service.aggregate(amount);
    }

    @GetMapping("/aggregate/character/{amount}")
    public Future<List<SourcePlayer>> aggregateSourceCharacter(@PathVariable("amount")Integer amount) {
        LOG.info("Find the aggregate source char for {}", amount);
        return service.aggregateBySourceCharacter(amount);
    }

    @GetMapping("/find/methods/{amount}")
    public Future<List<MethodsAmount>> findMostCommonMethods(@PathVariable("amount")Integer amount) {
        LOG.info("Find the most common methods for {}", amount);
        return service.findMostCommonMethods(amount);
    }
}
