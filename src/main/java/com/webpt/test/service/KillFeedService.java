package com.webpt.test.service;

import com.webpt.test.model.KillFeedPayload;
import com.webpt.test.model.Payload;
import com.webpt.test.model.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class KillFeedService {
    private Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired
    private RestTemplate restTemplate;

    @Value("${webpt.killfeed.url:default}")
    private String killfeedUrl;

    public List<Payload> retrieveKillFeed() {
        LOG.info("Retrieving for URL : {}", killfeedUrl);
        KillFeedPayload payload = null;
        try {
            payload = restTemplate.getForObject(killfeedUrl, KillFeedPayload.class);

            // We only want to return if the payload isnt errored out
            if (Status.successful.equals(payload.status)) {
                return payload.payloads;
            }
            // Else lets log the errors
            if (Status.failed.equals(payload.status)) {
                LOG.error("We failed to retrieve the payload items for reasons: {}" + payload.reason);
            }
            // else its a general exception
            else {
                LOG.error("General Exception, Status Code : {}, Message: {}", payload.error, payload.message);
            }
        } catch (HttpServerErrorException e) {
            LOG.error("Error retrieving from the server : " + e.getStatusCode() + " / " + e.getMessage());
        } catch(Exception e) {
            // I don't expect this to happen often
            LOG.error("Other exception : " + e.getMessage());
        }
        return new ArrayList<Payload>();
    }
}
