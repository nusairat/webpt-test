package com.webpt.test.service;

import com.webpt.test.domain.MethodsAmount;
import com.webpt.test.domain.SourcePlayer;
import com.webpt.test.model.KillFeedPayload;
import com.webpt.test.model.Payload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * This will retrieve and do some aggregation of the records.
 */
@Service
public class RetrieveRecordsService {

    private Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired
    KillFeedService killFeedService;

    @Autowired
    Executor taskExecutor;

    public Future<List<Payload>> aggregate(int amount) {
        LOG.info("Retrive {} records", amount);

        List<CompletableFuture<List<Payload>>> allPayloadsFuture = getCompletableFutures(amount);

        return CompletableFuture.allOf(allPayloadsFuture.toArray(new CompletableFuture<?>[allPayloadsFuture.size()]))
                .thenApply(v ->
                            allPayloadsFuture
                            .stream()
                            .map(CompletableFuture::join)
                            .flatMap(l -> l.stream())
                            .collect(toList())
                );
    }

    /**
     * This will take an amount to query against and find the most common methods used for destruction.
     * This is irrelevant of damage.
     */
    public Future<List<MethodsAmount>> findMostCommonMethods(int amount) {
        LOG.info("Retrive {} records for findMostCommonMethods", amount);

        List<CompletableFuture<List<Payload>>> allPayloadsFuture = getCompletableFutures(amount);

        return CompletableFuture.allOf(allPayloadsFuture.toArray(new CompletableFuture<?>[allPayloadsFuture.size()]))
                .thenApply(v ->
                        allPayloadsFuture.stream()
                                .map(CompletableFuture::join)
                                .flatMap(l -> l.stream())
                                .collect(Collectors.groupingBy(Payload::getMethod, Collectors.counting()))
                                .entrySet()
                                .stream()
                                .map(e -> new MethodsAmount(e.getKey(), e.getValue()))
                                .sorted(Comparator.comparing(MethodsAmount::getAmount).reversed())
                                .collect(toList())
                );
    }

    /**
     * This will aggregate all the damages by each source chacter based on the amount of records we atempt to find.
     */
    public Future<List<SourcePlayer>> aggregateBySourceCharacter(int amount) {
        LOG.info("Retrive {} records for aggregateBySourceCharacter", amount);

        List<CompletableFuture<List<Payload>>> allPayloadsFuture = getCompletableFutures(amount);

        return CompletableFuture.allOf(allPayloadsFuture.toArray(new CompletableFuture<?>[allPayloadsFuture.size()]))
                .thenApply(v ->
                            allPayloadsFuture.stream()
                            .map(CompletableFuture::join)
                            .flatMap(l -> l.stream())
                            .collect(Collectors.groupingBy(
                                    Payload::getSourceCharacter,
                                    Collectors.reducing(
                                            0,
                                            Payload::getDamage,
                                            Integer::sum)))
                            .entrySet()
                            .stream()
                            .map(e -> new SourcePlayer(e.getKey(), e.getValue()))
                            .sorted(Comparator.comparing(SourcePlayer::getSourceCharacter))
                            .collect(toList())
                );
    }

    private List<CompletableFuture<List<Payload>>> getCompletableFutures(int amount) {
        List<CompletableFuture<List<Payload>>> allPayloadsFuture = new ArrayList<CompletableFuture<List<Payload>>>(amount);
        // bleh better way to do this ...
        // but our list will have all the async items to run
        for (int i = 0; i < amount; i++) {
            allPayloadsFuture.add(CompletableFuture.supplyAsync(() -> killFeedService.retrieveKillFeed(), taskExecutor));
        }
        return allPayloadsFuture;
    }
}
