package com.webpt.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Payload {
    public int damage;
    public String method;
    public String platform;
    public String region;

    @JsonProperty(value = "source_character")
    public String sourceCharacter;

    @JsonProperty(value = "source_player_id")
    public String sourcePlayerId;

    @JsonProperty(value = "target_character")
    public String targetCharacter;

    @JsonProperty(value = "target_player_id")
    public String target_player_id;

    // Needed for our stream aggregators
    public String getSourceCharacter() {
        return sourceCharacter;
    }

    public String getMethod() {
        return method;
    }

    public int getDamage() {
        return damage;
    }
}
