package com.webpt.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class KillFeedPayload {

    @JsonProperty(value = "payload")
    public List<Payload> payloads;

    // Whether it was successful
    public String reason;
    public Status status;

    // Seems to occur when we have Exceptions
    public String error;
    public String message;
}
